import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Form } from '../models/form';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private Http: HttpClient) { }
  postForm(form: Form){
    return this.Http.post(`${environment.url}form/create`, form) as Observable<Form>;
  }
}
