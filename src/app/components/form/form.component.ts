import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Form } from 'src/app/models/form';
import { ApiService } from 'src/app/services/api.service';
import { ThemePalette } from '@angular/material/core';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  email = new FormControl('', [Validators.required, Validators.email]);

  result: Form[]= [];
  userForm: FormGroup;
  userInput: any; 
  dateInput: Date;
  addressInput: any;
  ssInput: number; 
  ss = [/[0-9]/, /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/,'-',/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/];
  bdInput: number; 
  emailInput: any; 
  phoneInput: number;
  phone = ['(',/[0-9]/, /[0-9]/, /[0-9]/,')', /[0-9]/, /[0-9]/, /[0-9]/,'-',/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/];
  cityInput: string;
  eInput: any;
  toInput: number; 
  fromInput: number; 
  branchInput: string;
  dischargeInput: number;
  stateList = [
    {value:'option-1', viewValue:'Az'},
    {value:'option-2', viewValue:'Al'},
    {value:'option-3', viewValue:'Ak'},
  ];

  constructor(private fb: FormBuilder, private formservice: ApiService) { }
  ngOnInit(): void {
  }

  getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'You must enter a value';
    }
    return this.email.hasError('email') ? 'Not a valid email' : '';
  }

  createForm(){
    return this.fb.group({
      id: [''],
      fullName: ['']
    });
  }
  createUserForm(){
    const form = new Form();
    form.fullName = this.userForm.controls.fulltName.value;
    this.formservice
    .postForm(form)
    .toPromise()
    .then((res) => {
      this.result.push(res);
    });
  }
  submitInfo = () => {
    const form: Form = {
      fullName: this.userInput,
    };
    this.formservice.postForm(form).toPromise().then((res) => {
    console.log(res)
    })
    .catch((err) => {
      console.log(err);
    });
  }


}
  